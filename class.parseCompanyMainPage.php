<?php
class ParseCMP {
	
	// get the location where cURL is installed
	public function getCUrlLocation(){
		try {
			$fileContents = parse_ini_file("editableFiles/configFile.ini");
			return $fileContents["cUrlLocation"];
		} catch (Exception $e) {
			error_log($e->getMessage(),3,"/var/tmp/error.log");
			error_log("\n");
		}
	}
	
	public function getEmployeeLinksData($html){
		$output;
		try {
			foreach ($html->find('.empLinks') as $link){
				foreach ($link->find('.reviews') as $reviews){
					$key =(string) $reviews->childNodes(1);
					$value =(string) $reviews->childNodes(0);
			
					$output[$key] = $value;
				}
				foreach ($link->find('.salaries') as $salaries){
					$key =(string) $salaries->childNodes(1);
					$value =(string) $salaries->childNodes(0);
						
					$output[$key] = $value;
				}
				foreach ($link->find('.interviews') as $interviews){
					$key =(string) $interviews->childNodes(1);
					$value =(string) $interviews->childNodes(0);
				
					$output[$key] = $value;
				}				
			}
		} catch (Exception $e) {
			error_log($e->getMessage(),3,"/var/tmp/error.log");
			error_log("\n");
		}
		return $output;
	}
	
	public function getRatings($html){
		$output = array();
		try {
			foreach ($html->find('.ratingInfo') as $r){
				$i = 0;
				foreach($r as $p){
					echo $r->childNodes($i);
					$i++;
				}
			}
		} catch (Exception $e) {
			error_log($e->getMessage(),3,"/var/tmp/error.log");
			error_log("\n");
		}
		return $output;
	}
	
	public function getAditionalInfo($html){
		$output;
		try {
			foreach ($html->find('div[id=EmpBasicInfo].module') as $info){
				foreach ($info->find('div[id=EmpBasicInfo]') as $info){
					foreach ($info->find('div[id=InfoDetails]') as $info){
						for($i=0;$i<sizeof($info->children);$i++){
							foreach($info->childNodes($i)->find('strong') as $s)
								echo $s;
							foreach($info->childNodes($i)->find('span') as $span)
								echo " ".$span;
							echo '<br/>';
						}
					}
				}
			}
		} catch (Exception $e) {
			error_log($e->getMessage(),3,"/var/tmp/error.log");
			error_log("\n");
		}
		return $output;
	}
	
	public function getEmployerId($html){
		foreach($html->find('a') as $a){
			if(strpos($a->href, "employerId=") != FALSE){
				$temp = explode("employerId=",$a->href);
				$temp = explode("&",$temp[1]);
				return $temp[0];
			}
		}
	}
	
	public function getRatingsPopupLink($html){
		try {
			$fileContents = parse_ini_file("editableFiles/configFile.ini");
			return $fileContents["ratingPopupLink_part1"].$this->getEmployerId($html).$fileContents["ratingPopupLink_part2"];
		} catch (Exception $e) {
			error_log($e->getMessage(),3,"/var/tmp/error.log");
			error_log("\n");
		}
	}
	
	public function parseRatings($ratings){
		$output;
		$count = 0;
		try{
			$ratings = str_replace("{","", $ratings);
			$ratings = str_replace("}", "", $ratings);
			$ratings = str_replace("\"", "", $ratings);
			$elements = explode("]",$ratings);
			$elements = explode("[", $elements[0]);
			$elements = explode(",", $elements[1]);

			
			foreach ($elements as $e){
				switch ($count){
					case 0: $count++;
						break;
					case 1: $count++;
						$element = explode(":", $e);
						$key = $element[1];
						break;
					case 2: $count++;
						$element = explode(":", $e);
						$value = $element[1];
						$output[$key] = $value;
						$count = 0;
						break;
				}
				
			}
		} catch (Exception $e) {
			error_log($e->getMessage(),3,"/var/tmp/error.log");
			error_log("\n");
		}
		return $output;
	}
	
	public function getStarsCountLink($html){
		try {
			$fileContents = parse_ini_file("editableFiles/configFile.ini");
			return $fileContents["starsCount_part1"].$this->getEmployerId($html).$fileContents["starsCount_part2"];
		} catch (Exception $e) {
			error_log($e->getMessage(),3,"/var/tmp/error.log");
			error_log("\n");
		}
	}
	public function getStarsCount($stars){
		$output;
		try{
			$stars = str_replace("{", "", $stars);
			$stars = str_replace("}", "", $stars);
			$stars = str_replace("\"", "", $stars);
			$stars = str_replace("[", "", $stars);
			$stars = str_replace("]", "", $stars);
			$elements = explode("labels:", $stars);
			$elements = explode(",values:", $elements[1]);
			$elements = implode(",", $elements);
			$elements = explode(",",$elements);
			$keySize = count($elements)/2;
			for($i=0;$i<$keySize;$i++){
				$output[$elements[$i]] = $elements[$i+$keySize]; 
			}
		} catch (Exception $e) {
			error_log($e->getMessage(),3,"/var/tmp/error.log");
			error_log("\n");
		}
		return $output;
	}
}