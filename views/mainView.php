<html>
	<head>
		<title>Main Page</title>
	</head>
	<body>
		<form action="main.php" method="post">
			<div style="padding-top:15px">
				<label>Get company data using API call</label>
				<input type="submit" id="apiBtn" value="Fetch" name="apiBtn">
			</div>
			<div style="padding-top:15px">
				<label>Goto Edit Files Page</label>
				<input type="submit" id="editPageBtn" value="Go" name="editPageBtn">
			</div>
			<div style="padding-top:15px">
				<label>Download API Output File</label>
				<input type="submit" id="downloadAPIBtn" value="Download" name="downloadAPIBtn">
			</div>
			<div style="padding-top:15px">
				<label>Download API Response Status File</label>
				<input type="submit" id="downloadAPIResponseBtn" value="Download" name="downloadAPIResponseBtn">
			</div>
			<div style="padding-top:15px">
				<label>Download API Filter Format File</label>
				<input type="submit" id="downloadFilterFormatBtn" value="Download" name="downloadFilterFormatBtn">
			</div>
		</form>
	</body>
</html>